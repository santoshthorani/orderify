<?php
namespace App\Http\Controllers\Shopify;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItems;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Revolution\Google\Sheets\Facades\Sheets;

class OrderWebhookController extends Controller
{
    
/**
 * Creates a new order.
 *
 * @param Request $request The request object containing the order data.
 * @return \Illuminate\Http\Response The response containing the order ID.
 */
    public function newOrder(Request $request)
    {
       $tab_name = $this->createTabIfNotExists();

        $order_id=$this->processOrder($request,$tab_name);

        return response()->make($order_id,200);
    }
    /**     
     *  Create a tab
     *  Create a new sheet (tab withing a spread sheet) with the name of current month
     *  For example if the current month is January, the sheet name will be January
     */
    private function createTabIfNotExists(){
        $tab_name =Carbon::now()->format('F');
        $sheet = Sheets::spreadsheet(env("SPREAD_SHEET_ID",''));
        try {
            $sheet->addSheet($tab_name);
            $sheet->sheet($tab_name);
        } catch (\Exception $e) {
            if($e->getCode() == 400){
                $sheet->sheet($tab_name);
            }

        }
        return $tab_name;
    }
    /**
     * Process the order
     * @param Request $request The request object containing the order data.
     * @param string $tab_name The name of the tab to append the order data to.
     * @return string The order ID.
     * I have extracted the data as per our requirements
     */
    private  function processOrder(Request  $request,$tab_name){

        $payload = json_decode($request->getContent(), true);
        $order_id = $payload['id'];
        $date = date('d/m/Y', strtotime($payload['created_at']));
        $buyer_name = $payload['shipping_address']['name'];
        $address = $payload['shipping_address']['address1'];
        $city = $payload['shipping_address']['city'];
        $contact_no = $payload['shipping_address']['phone'];
        $total_amount = $payload['total_price'];
        $order_no = $payload['name'];
        $note = $payload['note'];
        $financial_status= $payload['financial_status'];
        if(isset($payload['gateway'])){
        $gateway= $payload['gateway'];
        }

        $utmParams = parse_url($payload['landing_site'], PHP_URL_QUERY);
        if(!isset($utmParams)){
            $utmParams=parse_url($payload['referring_site'], PHP_URL_QUERY);
        }

        $order = new Order();
        $order->order_id = $order_id;
        $order->date = $date;
        $order->buyer_name = $buyer_name;
        $order->address = $address;
        $order->destination_city = $city;
        $order->contact_no = $contact_no;
        $order->total_amount = $total_amount;
        $order->order_no = $order_no;
        $order->note = $note;
        $order_array = array('Date' => $date);
        $orders = [];
        $order->payload = $request->getContent();

        /* Save the order 
        *  If the order is saved successfully, save the order items
           and append the order data to the sheet
        */
//        if ($order->save()) {
            $orderId = $order->id;
            
            foreach ($payload["line_items"] as $item) {
                $orderItem = new OrderItems();
                $product_name = $item["title"];
                $orderItem->order_id = $orderId;
                $orderItem->product_id = $item["product_id"];
                $orderItem->product_name = $product_name;
                $order_array['Product Name'] = $product_name;
                $variant = $item["variant_title"];
                $customise = "";
                $itemQty =$item["fulfillable_quantity"];
                if (!empty($variant)) {
                    $customise .= $variant
                        . PHP_EOL;
                }
                if (!empty($item["properties"])) {
                    foreach ($item["properties"] as $property) {
                        $customise .= $property["name"] . " : " . $property["value"] . PHP_EOL;
                    }
                }
                $order_array['Customise/Size'] = $customise;
                $order_array['Item Qty'] =$itemQty ;
                $order_array['Buyer Name'] = $buyer_name;
                $order_array['Address'] = $address;
                $order_array['destination city'] = $city;
                $order_array['Contact Number'] = $contact_no;
                $order_array['amount'] = $total_amount;
                $order_array['Financial Status'] = $financial_status;
                $order_array['Gateway'] = !isset($gateway) ? "" : $gateway;
                $order_array['order#'] = $order_no;
                $order_array['confirmed'] = "";
                $order_array['or not'] = "";
                $order_array['instruction'] = $note;
                $order_array['source'] = $utmParams;
                $orderItem->customise = $customise;
                $orderItem->item_qty =$itemQty;

             //   $orderItem->save();
                /* 
                * We required to create new row with every product in the order
                * we can also append the product items in one string but as per requirmenets
                * this is more intuitive.
                */
                array_push($orders, $order_array);

            }
            Sheets::spreadsheet(env("SPREAD_SHEET_ID", ''))->sheet($tab_name)->append($orders);
//        }
        return $order_id;
    }
}

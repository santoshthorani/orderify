# Orderify - Shopify Orders to your Google Sheet

Orderify automates order synchronization between Shopify and Google Sheets, resulting in a
significant reduction of team workload by saving up to 20 hours per week.

## Requirments

* PHP >=8.1
* Laravel >=10.0

## Installation
## Composer
##
        composer install
## Laravel
1. Run `php artisan vendor:publish --tag="google-config"` to publish the google config file
##
        //  config/google.php
        //  OAuth
        'client_id'        => env('GOOGLE_CLIENT_ID', ''),
        'client_secret'    => env('GOOGLE_CLIENT_SECRET', ''),
         'redirect_uri'     => env('GOOGLE_REDIRECT', ''),
         'scopes'           => [\Google\Service\Sheets::DRIVE, \Google\Service\Sheets::SPREADSHEETS],
         'access_type'      => 'online',
         'approval_prompt'  => 'auto',
         'prompt'           => 'consent', //"none",         "consent", "select_account" default:none

         // or Service Account
         'file'    => storage_path('credentials.json'),
         'enable'  => env('GOOGLE_SERVICE_ENABLED', true),
2. Get API Credntials from (https://developers.google.com/console)  
Enable `Google Sheet API`, `Google Drive API`.
3. Configure .env, copy .env.example and rename to .env
##
        DB_DATABASE=
        DB_USERNAME=
        DB_PASSWORD=
        #Spread Sheet ID where the data will be stored
        SPREAD_SHEET_ID =
        #Google Credential & Configuration
        GOOGLE_APPLICATION_NAME=""
        GOOGLE_CLIENT_ID=
        GOOGLE_CLIENT_SECRET=
        GOOGLE_DEVELOPER_KEY=
        GOOGLE_SERVICE_ENABLED=

        #Add Google Service Account JSON file
        GOOGLE_SERVICE_ACCOUNT_JSON_LOCATION=../storage/sheets-service- account.json
## Run App
        php artisan serve
you will see url in terminal i.e  http://127.0.0.1:8000
Api endpoint url will
##
         your-url/api/orders
### Note
I have commneted line which saves the data to database. You need to uncomment the line to save data in database.
Here is the structure of my tables
### Order Table
![Order Table ](images/orders_table.png) 
![Order Table ](images/order_item_table.png)

You need to create table as per your needs and call model as per your needs.

## Setup Shopify WebHook
1. Open your shopify admin
2. Go to Settings -> Notifications
3. Click on **Webhooks**
4. Click on **+ Create webhook**
5. Select  your event, in this case we select **Order Creation**, make sure format is JSON
6. Add your Api endpoint here (make sure it is https).
7. Selection webhook api version. I always prefer (latest).
8. Click **Save**
9. After this, you can see "Your webhooks will be signed with".... This is a digital signature which helps you to verify webhook was sent by Shopify.
![Shopify Webhook Setup ](images/shopify_webhook.png)

## Shopify Webhook Flow
When new order is received, the Shopify will trigger your endpoint with json data. You are required ***to response back with 200 within few seconds***. If failed ***It will again trigger the endpoint***. After few failed attempts, the Shopify removes the webhook.

## Google Sheet
You can see in our code, we have few empty values against some columns. This is because we need to follow the order of Columns names in the sheet otherwise newly inserted row will be added begining from the end of the column.
![Google Sheet Data ](images/google_sheet_ss.png)

## TODO Items
* **Verify the webhook**  
We need to verify the webhook was sent from Shopify. Each webhook request includes a base64-encoded X-Shopify-Hmac-SHA256 header.
* API Versioning  
We need to do API versioning to make changes to our API without creating problems.
* Prevent duplicates orders.
* Implementating Clean Code Architecture
* Unit Tests
